
# Samenvatting Elektriciteit en magnetisme


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/0BmLODc3K4g?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Samenvatting van wetten van Maxwell voor velden.

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/LILKAhL6gdU?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Krachten, energie in componenten.
