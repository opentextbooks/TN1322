# Wet van Gauss
In het vorige hoofdstuk hebben we gezien hoe je met de wet van Coulomb elektrische velden kan uitrekenen door ladingsverdelingen.
In dit hoofdstuk laten we zien dat het in gevallen van hoge symmetrie vaak veel makkelijker en eleganter kan met de wet van Gauss. 
Deze wet van Gauss is de eerste van de vier fundamentele wetten van Maxwell waarmee we uiteindelijk het hele elektromagnetisme zullen beschrijven.


## Elektrische flux


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/XSGPAXwcC4o?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Door schetsen te maken van elektrische veldlijnen, kan je een goed beeld krijgen van het begrip  elektrische flux.

## Wet van Gauss


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/4QHhnHgoYts?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




De  wet van Gauss beschrijven we in dit boek als een integraal over de elektrische flux.


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/OeDW58Af-x8?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Als eerste twee voorbeelden bekijken we hoe je makkelijk met de wet van Gauss het elektrische veld kan berekenen van een geladen plaat en draad. 
Dit gaat goed omdat we een situatie met respectievelijk vlak- en cylindersymmetrie beschrijven.
Het doorrekenen van deze twee ladingsverdelingen zou heel lastig zijn en veel tijd kosten met de wet van Coulomb.


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/1B_qgfQNW-8?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Als volgend voorbeeld berekenen het elektrische veld in en buiten een  geladen bolschil.
Dit is nu een vrij eenvoudige berekening.


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/at-6zmCxaxA?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Om te laten zien hoeveel ingewikkelder dit zou zijn met  wet van Coulomb, voeren we deze berekening ook uit. 
Dit is voor dubbele BSc-studenten een uitdaging die ze zelf mogen proberen op te lossen, voordat ze naar deze video kijken.

## Wet van Gauss toegepast op geleiders


<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/t3SJCGdbuhE?si=_4laS7pkvf5ghBtI?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Tot nu toe hebben we gekeken naar de velden in vacu&uuml;m. 
Materialen, zoals geleiders, be&iuml;nvloeden elektrische velden. 
In een geleider is het elektrische veld nul in een elektrostatische situatie.


<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/BHiFkeCZImU?si=AqUsFDWeBo8lGZ4y?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Dat het veld in een geleider nul is, heeft gevolgen voor de ladingsverdeling in de geleider en daardoor voor het  veld om de geleider heen.
Zo blijkt het veld op het grensvlak altijd loodrecht hierop te staan, buiten de geleider om.


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/11BL47PklLw?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Een elegante demonstratie laat zien hoe we met eenvoudige componenten de wet van Gauss in en om een geladen pan zichtbaar kunnen maken.

## Tentamenopgave voor bolsymmetrisch probleem


<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/BhS0OnBylHo?align=center"
    frameborder="0"
    allowfullscreen
></iframe>




Tot slot berekenen we een oude  tentamenvraag, waarbij de wet van Gauss heel handig van pas komt.

