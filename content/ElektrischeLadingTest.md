
# Elektrische lading, krachten en velden
In dit hoofdstuk laten we zien hoe je met de wet van Coulomb de krachten tussen elektrische ladingen kan uitrekenen.
Deze krachten kan je ook weer beschrijven aan de hand van de elektrische velden veroorzaakt door een lading.
De wet van Coulomb is niet alleen geldig voor puntladingen, maar kan ook voor continue verdelingen worden uitgerekend.
## De wet van Coulomb voor puntladingen

<iframe width="600" height="400" src="https://www.youtube.com/embed/9gpHy_6JncE" frameborder="0" allowfullscreen></iframe>

<iframe width="600" height="400" src="https://www.youtube.com/embed/9gpHy_6JncE" frameborder="0" allowfullscreen></iframe>

De analogie met zwaartekracht kan je gebruiken om zowel elektrische lading als de kracht tussen puntladingen uit te leggen.

<iframe width="600" height="400" src="https://www.youtube.com/embed/owOMgs3--IM" frameborder="0" allowfullscreen></iframe>

Als toepassing van de wet van Coulomb rekenen we de verhouding uit tussen de elektrische en zwaartekracht tussen een elektron en proton uit.

## Superpositie
<iframe width="600" height="400" src="https://www.youtube.com/embed/AfyDUHlJ4Wc" frameborder="0" allowfullscreen></iframe>

Elektrische velden kan je simpelweg bij elkaar optellen. We noemen dit principe Superpositie.

<iframe width="600" height="400" src="https://www.youtube.com/embed/wLczo55Qkb0" frameborder="0" allowfullscreen></iframe>

Als voorbeeld berekenen we de totale kracht die 2 regendruppels op een derde uitoefenen.

<iframe width="600" height="400" src="https://www.youtube.com/embed/FCNllgpDnXg" frameborder="0" allowfullscreen></iframe>

Om gevoel te krijgen voor superpositie kunnen jullie hierboven kijken naar een spel waarin dit principe wordt toegepast. Het is natuurlijk nog leuker om dit zelf te <a href="https://phet.colorado.edu/nl/simulations/electric-hockey" target="_blank">spelen</a>.

<iframe width="600" height="400" src="https://www.youtube.com/embed/-xeCMah_7Js" frameborder="0" allowfullscreen></iframe>

Een van de meest belangrijke toepassingen van superpositie is het geval van de dipool. 
Dipolen komen heel veel voor in de natuur. 
Veel van de interacties tussen moleculen worden met dipolen beschreven.


## Elektrisch veld van water
### Wet van Coulomb herschreven

Als toepassing van de wet van Coulomb en illustratie van superpositie berekenen we het elektrische veld van het water molecuul. 

$$
\vec{E} = \sum_i \frac{k q_i}{r_i^2} \hat{r_i}
$$

Waarin $r_i$ de co&ouml;rdinaten van de atomen beschrijft en $q_i$ hun lading.
Om de $x$- and $y$-componenten handig te berekenen herschrijven we de wet van Coulomb

$$
E_x = \sum_i \frac{k q_i r_x}{r_i^3} \,
$$

en

$$
E_y = \sum_i \frac{k q_i r_y}{r_i^3} \,
$$

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt

def LawCoulomb(x,y,xc,yc,q): 
    # x and y are coordinates at which field is calculated
    # xc and yc are coordinates at which the charge is located with the charge q
    k=9e9 # N m^2 / C^2
    rx=(x-xc)*1e-12 # convert from pm to m
    ry=(y-yc)*1e-12 # convert from pm to m
    r=np.sqrt(rx**2 + ry**2)
    Ex=k*q*rx*r**(-3)
    Ey=k*q*ry*r**(-3)
    return Ex, Ey
```

### Structuur van het water molecuul

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/H2O_2D_labelled.svg/1200px-H2O_2D_labelled.svg.png" width=30% height=30%> 

We gebruiken de co&ouml;rdinatiehoek en de afstand tussen het zuurstof en waterstofatoom om de co&ouml;rdinaten in het $xy$-vlak te berekenen. We nemen aan dat de elektronen van het waterstof volledig op het zuurstofatoom zitten.


```{code-cell} ipython3
# Charge distribution on H2O
e=1.6e-19 # Coulomb
q_O=-2*e # assume O has 2 extra electrons
q_H1=1*e # assume H hs 1 electron less
q_H2=1*e

# Coordinates H2O
rOH=95.84 # pm
theta=104.45 # degree
theta05=theta*np.pi/360. # theta/2 in radian
x_O=0 # place oxygen at origin
y_O=0
x_H1=-rOH*np.sin(theta05)
y_H1=-rOH*np.cos(theta05)
x_H2=rOH*np.sin(theta05)
y_H2=-rOH*np.cos(theta05)
```
### Elektrisch veld-rooster
We defini&euml;ren het rooster om het veld op te berekenen in picometer. Het is handig om een even aantal roosterpunten te nemen om te voorkomen dat het zuurstofatoom op een roosterpunt ligt en er een divergentie in de veldberekening optreedt.

```{code-cell} ipython3
# Define lattice for field calculation
nr=50
r_max=500
axis=np.linspace(-r_max,r_max,nr)
x=np.tile(axis,(nr,1))
y=np.tile(axis,(nr,1)).transpose()
```
### Superpositie
We gebruiken het superpositie principe om het totale veld te berekenen als de som van de bijdrages van de 3 individuele atomen.

```{code-cell} ipython3
# Calculation of the fields of the 3 atoms
[E_Ox,E_Oy]=LawCoulomb(x,y,x_O,y_O,q_O)
[E_H1x,E_H1y]=LawCoulomb(x,y,x_H1,y_H1,q_H1)
[E_H2x,E_H2y]=LawCoulomb(x,y,x_H2,y_H2,q_H2)
Ex=E_Ox+E_H1x+E_H2x # Superposition
Ey=E_Oy+E_H1y+E_H2y
```
### Vector plot lijkt op dat van een dipool
We plotten de logaritme van de veldsterkte om de richting van zowel de lagere als de hogere velden zichtbaar te maken. Op grote afstanden van het molecuul is het veld gelijk aan dat van een dipool.

```{code-cell} ipython3
#logaritmic scaling of E-field to make it nicely looking
E=np.sqrt(Ex**2 + Ey**2)
Emax=np.amax(E)
Emin=np.amin(E)
scale = np.log(1000*E/Emin)/E # factor of 1000 increases lengths of lowest E-fields to visibility
Exl=Ex*scale
Eyl=Ey*scale
fig = plt.figure(figsize=(15, 15))
ax = fig.subplots()
plt.quiver(x,y,Exl,Eyl)
ax.set_xlabel('x [pm]')
ax.set_ylabel('y [pm]')
ax.set_title('electrical field H$_2$O')
ax.set_aspect('equal', adjustable='box')
ax.text(x_O, y_O, r'O',ha='center',va='center',color='r',fontsize='xx-large') # negative oxygen
ax.text(x_H1, y_H1, r'H',ha='center',va='center',color='r',fontsize='xx-large') # positive hydrogen
ax.text(x_H2, y_H2, r'H',ha='center',va='center',color='r',fontsize='xx-large') # positive hydrogen
plt.plot([x_H1,x_O,x_H2],[y_H1,y_O,y_H2],'r')
plt.show()
```

## Continue ladingsverdelingen

<iframe width="600" height="400" src="https://www.youtube.com/embed/-DhzNkE2Afg" frameborder="0" allowfullscreen></iframe>

Veel problemen kan je wiskundig veel makkelijker uitrekenen als je gebruikt maakt van symmetrie. Veel termen kan je vaak op grond van symmetrie weg laten, waardoor berekeningen veel korter worden. Het principe van superpositie kan uitgebreid worden naar continue ladingsverdelingen door over alle lading te integreren.

<iframe width="600" height="400" src="https://www.youtube.com/embed/vdQ0yulzPSE" frameborder="0" allowfullscreen></iframe>

Als voorbeeld is hier de berekening van het elektrische veld van een geladen ring met behulp van de wet van Coulomb.

## Integreren in de natuurkunde
We merken dat veel studenten het toepassen van integreren in elektriciteit en magnetisme moeilijk vinden.
Integreren in de natuurkunde is vaak over massa of lading in plaats van over ruimtelijke co&ouml;rdinaten zoals je op de middelbare school met wiskunde gewend bent. 
Je moet leren om de integraal over bijvoorbeeld lading om te zetten naar een handige ruimtelijke co&ouml;rdinaat. 
Wat handig is hangt helemaal af van de geometrie van een probleem. 
Hieronder geven we drie voorbeelden van omzettingen van integratieco&ouml;rdinaat.

<iframe width="600" height="400" src="https://www.youtube.com/embed/vMkwLL4_2jI" frameborder="0" allowfullscreen></iframe>

Zo kan je lading bijvoorbeeld over een rechte lijn integreren.

<iframe width="600" height="400" src="https://www.youtube.com/embed/aabm7p3kGLY" frameborder="0" allowfullscreen></iframe>

Integreren over een ring of ringsegment is ook mogelijk.

<iframe width="600" height="400" src="https://www.youtube.com/embed/vuJiiBJWpKM" frameborder="0" allowfullscreen></iframe>

Integreren over een schijf is net wat complexer.

## Demonstratie
<iframe width="600" height="400" src="https://www.youtube.com/embed/5u-WR7oNJ00" frameborder="0" allowfullscreen></iframe>

De Coulomb-krachten klinken abstract, maar kunnen in een demonstratie goed zichtbaar worden gemaakt.

