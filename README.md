Introductie Elektriciteit en Magnetisme 2023
===================================================

Dit wordt later aangevuld.

## Het schrijven van het boek (in het Engels)
The book is written in an extended version of the Markdown language used by Jupyter Book.
Documentation for this language is available [here](https://jupyterbook.org/content/myst.html),
and there is a cheat sheet [here](https://jupyterbook.org/reference/cheatsheet.html).
You can use LaTeX equations and reference them, create figures, etc.
The Markdown source files are located in the `content` folder (apart from the
homepage, `index.md`). The way these files
appear as chapters in the book is specified in the `_toc.yml` file. If you want
to add chapters or change the structure of the book, you should read the
documentation [here](https://jupyterbook.org/customize/toc.html).

Ordered lists in Markdown are a bit tricky to work with. There's a CSS file in
the `_static` folder that makes all ordered lists look like a), b), c), etc.
This means that it is currently not possible to (simply) make ordered lists with numbers.
Also, if an ordered list is interrupted (e.g. by a normal paragraph or a figure),
this will reset the counter. Hence, if you start a new list, it will start as a),
even when you start it as `7.` or something. See
[this page](https://www.markdownguide.org/basic-syntax/#ordered-lists).
An artefact of this is visible at the bonus question on the bottom of Problem
Set 6 (Phase transitions).

The `_config.yml` file specifies things like the author, the logo and the
title of the book, and the way things should be executed or compiled. Also, the
line `hypothesis: true` enables [Hypothesis](https://jupyterbook.org/interactive/comments/hypothesis.html),
which is a way for students and
teachers to comment on pieces of text (when the book is published).
You simply need to create an account and then you can view and add comments.

The Python packages that you need to work with the book are listed in
`requirements.txt`.
Currently, there are some static plots written in `matplotlib` and some
interactive plots written in `plotly`.
The Markdown files that contain *code cells* with
executable code can also be opened as Jupyter Notebooks
(see [this page](https://jupyterbook.org/file-types/myst-notebooks.html)).
You can choose to show the code, hide the code, or
show the code in a drop-down box by specifying *tags* in the code cell.
If you want to edit the plots, I would recommend doing so from the Jupyter
Notebook interface. Editing code in the Markdown files themselves is rather
cumbersome.

## Het boek samenstellen (in het Engels)
To build the book, clone this repository and run `jupyter-book build .` in the repository root from a
shell or command prompt (if it doesn't work, check if your computer can find
`jupyter-book`, i.e. check if it is added to PATH). This
creates a new folder, `_build`, in which the HTML files are located; you can view
these in your browser. Currently, the `_build` folder is ignored by Git (specified
in `.gitignore`).

**IMPORTANT:** Building the book on Windows currently only works with Python 3.7 (due to
the execution of code in Problem set 6). If no code is executed, newer versions
of python work as well.
If you want to have multiple Python versions on your Windows machine,
you might want to use a [virtualenv](https://virtualenv.pypa.io/en/latest/user_guide.html)
to use a Python 3.7 installation only when you work on the book.

## Het boek publiceren (in het Engels)
...

## To-do
In short:

* Publish the website (check: http://tnw-nb-open-courses.tudelft.nl/NB2220/_build/html/index.html)
* Implement continuous integration from Gitlab repository
* Write documentation on publishing and make it available to anyone who needs it
* Expand the Book as needed, and add content to make the Book an independent and self-contained source for the course.
* Including one new chapter on the Ising Model and its connections to cell membranes and semi-flexible polymers
