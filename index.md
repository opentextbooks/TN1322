
Introductie Elektriciteit en Magnetisme
=======================================

Dit boek is geschreven voor het vak introductie Elektriciteit en Magnetisme in de BSc
Technische Natuurkunde aan de TU Delft. Deze cursus geeft een inleiding op het gebied van
elektriciteit en magnetisme en hun toepassingen in natuurkundig onderzoek. 
De inhoud loopt geheel parallel aan de standaard introductievakken die aan Amerikaanse universiteiten in het eerste jaar van de BSc worden gegeven.
Het gaat ook goed samen met bijna alle Amerikaanse leerboeken die hierbij meestal gebruikt worden.
DIt boek bevat de links naar verschillende video's, die de auteurs vooral hebben opgenomen tijdens de corona-lockdown in 2020.
Met dit boek plaatsen we deze video's in een duidelijker geheel, zodat studenten gericht ernaar kunnen zoeken.
Verder illustreren we sommige begrippen met korte berekeningen in Python.

Dit boek en de video's zijn interessant voor alle studenten aan het wetenschappelijke onderwijs die de basis van elektriciteit en magnetisme willen leren. Iedereen mag dit werk hergebruiken of remixen. Dit boek met bijbehorende video’s is gelicenseerd onder een [Creative Commons Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by-sa/4.0/) licentie, tenzij anders vermeld.
Deze website is een [Jupyter Book](https://jupyterbook.org/intro.html).
MarkDown-bronbestanden kunnen worden gedownload via de knop rechtsboven.

Het idee van dit boek is om het met elke run van de cursus uit te breiden en te verbeteren.
U kunt voor vragen of feedback een e-mail sturen naar W.G. Bouwman [at] tudelft [dot] nl.

## De docenten

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/UCb-b82tzLo?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Wim Bouwman

<iframe
    width="600"
    height="300"
    src="https://youtube.com/embed/DllcllOzaBY?si=9dJMVrpUFZpf2cFG?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Jacob Hoogenboom

<iframe
    width="600"
    height="300"
    src="https://www.youtube.com/embed/yJ2Qu7IOlDs?align=center"
    frameborder="0"
    allowfullscreen
></iframe>

Ron Haaksman

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
